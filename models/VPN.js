const mongoose = require("mongoose")

const schema = mongoose.Schema({
    flagurl: String,
    flagurl_small: String,
    provider: String,
    protection: String,
    countrycode: String,
    enabled: Boolean,
    down: Boolean,
    url: String,
    servername: String,
    ipv6: Boolean
})

module.exports = mongoose.model("VPN", schema)