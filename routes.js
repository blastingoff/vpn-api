const express = require("express");
const fs = require("fs");
const VPN = require("./models/VPN");
const router = express.Router();

let CLIENT_VERSION = "";
let CLIENT_DOWNLOAD = "";
let API_KEY = "";

fs.readFile("config.json", (err, data) => {
    if (err) throw err;
    var config = JSON.parse(data);

    // Set variables from our config.
    API_KEY = config.api_key;
    CLIENT_VERSION = config.client_version;
    CLIENT_DOWNLOAD = config.client_download;
});

function prepareAndSend(res, data) {
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(
        data,
        null,
        3)
    );
}

router.get("/getVersion", async (req, res) => {
    prepareAndSend(res,
        {
            version: CLIENT_VERSION,
            download: CLIENT_DOWNLOAD
        });
});

router.get("/servers", async (req, res) => {
    VPN.find().lean().exec(function (err, servers) {
        // Clean the data of MongoDB junk before sending out.
        let cleanedData = new Array();

        for (const serverId in servers) {
            let server = servers[serverId];

            // Goodbye!
            delete server["_id"];
            delete server["__v"];

            // Add the cleaned data for this server to our array.
            cleanedData.push(server);
        }
        prepareAndSend(res, cleanedData);
    })
});

router.post("/servers", async (req, res) => {
    let data = req.body;

    if (data.key && data.key == API_KEY) {
        const server = new VPN({
            flagurl: data.flagurl,
            flagurl_small: data.flagurl_small,
            provider: data.provider,
            protection: data.protection,
            countrycode: data.countrycode,
            enabled: data.enabled,
            down: data.down,
            url: data.url,
            servername: data.servername,
            ipv6: data.ipv6
        });

        // Save into our MongoDB database.
        await server.save();

        return res.status(200).send({ success: true, message: "Added server successfully." });
    } else {
        return res.status(403).send({ success: false, message: "Authentication failed." });
    }
});

module.exports = router;